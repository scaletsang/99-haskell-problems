module Arithmetic where

range :: Int -> Int -> [Int]
range n m
  | n == m = [n]
  | n < m = n : range (n+1) m
  | otherwise = n : range (n-1) m

--P31
isPrime :: Int -> Bool
isPrime 1 = True
isPrime x = isPrime' (range 2 (x-1)) x
  where isPrime' [] x = True
        isPrime' (n:nx) x =
          rem x n /= 0 && isPrime' nx x

--P32
gcd' :: Int -> Int -> Maybe Int
gcd' 0 _ = Nothing
gcd' _ 0 = Nothing
gcd' x y = return $ gcd'' x y
  where gcd'' x 0 = x
        gcd'' x y = gcd'' y (mod x y)

--P33
coprime :: Int -> Int -> Bool
coprime x y = gcd' x y == Just 1

--P34
phi :: Int -> Int
phi x = length $ filter (coprime x) [1..x]

--P36
packpf :: Int -> [[Int]]
packpf x = packInt $ primeFactorsOf x

--P37
primeFactorsOf :: Int -> [Int]
primeFactorsOf 0 = []
primeFactorsOf 1 = [1]
primeFactorsOf x = pf' x 2
  where pf' x n
          | n == x = [n]
          | mod x n == 0 = n: pf' (x `div` n) n
          | otherwise = pf' x (n+1)

packInt :: [Int] -> [[Int]]
packInt (x:xs) = packInt' x 1 xs
  where packInt' x n [] = [x:[n]]
        packInt' x n (y:ys) =
          if y == x then packInt' x (n+1) ys
          else (x: [n]) : packInt' y 1 ys
