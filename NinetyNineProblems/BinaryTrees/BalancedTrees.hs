module BinaryTrees.BalancedTrees where

data Tree a = Empty | Branch a (Tree a) (Tree a)

instance Show a => Show (Tree a) where
  show tree = showT 0 tree
    where showT n Empty = indent n ++ "Empty"
          showT n (Branch x l r) = indent n ++ unwords ["Branch", show x, printNext n l, printNext n r]
          indent n = "\n" ++ replicate n ' '
          printNext n t = showT (n + 4) t

-- P55
-- In a completely balanced binary tree, the following property holds for every node: The number of nodes in its left subtree and the number of nodes in its right subtree are almost equal, which means their difference is not greater than one.

-- Write a function cbal-tree to construct completely balanced binary trees for a given number of nodes. The predicate should generate all solutions via backtracking. Put the letter 'x' as information into all nodes of the tree.

cbalTree :: Int -> [Tree Char]
cbalTree 0 = []
cbalTree 1 = [branchX]
cbalTree 2 = [branchL 'x', branchR 'x']
cbalTree n
  | even n = permute $ cbalTree (halfN + 1)
  | otherwise      = permute $ cbalTree halfN
    where permute treeLs  = [treeX t1 t2 | t1 <- cbalTree halfN, t2 <- treeLs]
          halfN = n `div` 2

branch :: a -> Tree a
branch x = Branch x Empty Empty

branchL :: a -> Tree a
branchL x = Branch x (branch x) Empty

branchR :: a -> Tree a
branchR x = Branch x Empty (branch x)

tree :: a -> Tree a -> Tree a -> Tree a
tree _ x Empty = x
tree _ Empty x = x
tree x t1 t2   = Branch x t1 t2

branchX :: Tree Char
branchX = branch 'x'

treeX :: Tree Char -> Tree Char -> Tree Char
treeX = tree 'x'

-- P56
-- Let us call a binary tree symmetric if you can draw a vertical line through the root node and then the right subtree is the mirror image of the left subtree. Write a predicate symmetric/1 to check whether a given binary tree is symmetric. Hint: Write a predicate mirror/2 first to check whether one tree is the mirror image of another. We are only interested in the structure, not in the contents of the nodes.

isSymmetric :: Tree a -> Bool
isSymmetric Empty = True
isSymmetric (Branch _ t1 t2) = isIsomorphic (reflect t1) t2

isIsomorphic :: Tree a -> Tree a -> Bool
isIsomorphic Empty Empty = True
isIsomorphic (Branch _ t1 t2) (Branch _ t3 t4) = isIsomorphic t1 t3 && isIsomorphic t2 t4
isIsomorphic _ _ = False

reflect :: Tree a -> Tree a
reflect Empty = Empty
reflect (Branch x t1 t2) = Branch x (reflect t2) (reflect t1)

-- P57
-- Use the predicate add/3, developed in chapter 4 of the course, to write a predicate to construct a binary search tree from a list of integer numbers.

-- construct :: Ord a => [a] -> Tree a
-- construct = foldl f Empty
--   where f a x = 

-- insertBTreeNode :: Ord a => a -> Tree a -> Tree a
-- insertBTreeNode x Empty = branch x
-- insertBTreeNode x (Branch y t1 t2)
--   | x > y = 
--   | otherwise = 