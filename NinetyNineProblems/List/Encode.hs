module List.Encode where

import List.Operations ( pack )

-- P10
-- Run-length encoding of a list.
-- Use the result of problem P09 to implement the so-called run-length encoding data compression method. Consecutive duplicates of elements are encoded as lists (N E) where N is the number of duplicates of the element E.

-- Using P09 function
encode :: Eq a => [a] -> [(Int, a)]
encode [] = []
encode xs = (map f . pack) xs
  where f = \xs@(x:_) -> (length xs, x)

-- Direct counting solution
encodeD :: Eq a => [a] -> [(Int, a)]
encodeD []       = []
encodeD (x:xs) = encodeD' (1, x) xs
  where encodeD' t []  = [t]
        encodeD' t@(n, a) (x:xs)
          | x == a    = encodeD' (n + 1, a) xs
          | otherwise = t : encodeD' (1, x) xs

-- P11
-- Modified run-length encoding.
-- Modify the result of problem P10 in such a way that if an element has no duplicates it is simply copied into the result list. Only elements with duplicates are transferred as (N E) lists.
data Item a = Item a | Stack (Int, a)

instance Show a => Show (Item a) where
  show (Item x)  = "Item " ++ show x
  show (Stack x) = "Stack " ++ show x

encodeModified :: Eq a => [a] -> [Item a]
encodeModified [] = []
encodeModified xs = (map f . pack) xs
  where f [x]      = Item x
        f xs@(x:_) = Stack (length xs, x)
        f []       = undefined

-- Implementation of a unique List data type for P12 and P13.
-- The data type gives a better interface when composing a list that contains single items and tuples that represent a stack of consecutive repeating elements.
newtype ListA a = List [Item a]

instance Show a => Show (ListA a) where
  show (List x) = "List " ++ show x

instance Eq a => Eq (Item a) where
  (==) (Item a) (Item b)             = a == b
  (==) (Stack (_, a)) (Item b)       = a == b
  (==) (Item a) (Stack (_, b))       = a == b
  (==) (Stack (_, a)) (Stack (_, b)) = a == b

instance Eq a => Semigroup (ListA a) where
  -- (<>) :: List a -> List a -> List a
  (<>) (List []) x         = x
  (<>) x (List [])         = x
  (<>) (List xs) (List (y:ys)) = List $ combine xs y
      where combine [x] y = if x == y then combineItem x y : ys else x : y : ys
            combine (x:xs) y = x : combine xs y
            combine [] y = [y]
            combineItem x y = createItem (getItemFreq x + getItemFreq y) (runItem x)

runItem :: Item a -> a
runItem (Item x)  = x
runItem (Stack x) = snd x

getItemFreq :: Item a -> Int
getItemFreq (Item _)  = 1
getItemFreq (Stack x) = fst x

createItem :: Int -> a -> Item a
createItem 1 x = Item x
createItem n x = Stack (n, x)

instance Functor Item where
  fmap f (Item x)       = Item (f x)
  fmap f (Stack (n, x)) = Stack (n, f x)

instance Functor ListA where
  fmap f (List xs) = List $ map (fmap f) xs

instance Applicative ListA where
  pure x = List [Item x]
  -- (<*>) :: ListA (a -> b) -> ListA a -> ListA b
  (<*>) (List fs) (List xs) = List [fmap (runItem f) x | f <- fs, x <- xs]

-- P12
-- Decode a run-length encoded list.
-- Given a run-length code list generated as specified in problem P11. Construct its uncompressed version.

decode :: [Item a] -> [a]
decode []            = []
decode ((Item x):xs) = x : decode xs
decode ((Stack (n, x)):xs)
  | n == 0           = decode xs
  | otherwise        = x : decode (Stack ( n - 1, x):xs)

decodeList :: ListA a -> [a]
decodeList (List xs) = decode xs

-- P13
-- Run-length encoding of a list (direct solution).
-- Implement the so-called run-length encoding data compression method directly. I.e. don't explicitly create the sublists containing the duplicates, as in problem P09, but only count them. As in problem P11, simplify the result list by replacing the singleton lists (1 X) by X.

-- Using List data type for better composition interface
encodeList :: Eq a => [a] -> ListA a
encodeList = encodeDirectList

encodeDirectList :: Eq a => [a] -> ListA a
encodeDirectList = foldl (\ a x -> a <> pure x) (List [])

-- normal version
encodeDirect :: Eq a => [a] -> [Item a]
encodeDirect [] = []
encodeDirect (x:xs) = encode' (Item x) xs
  where encode' t    [] = [t]
        encode' t@(Item a) (x:xs)
          | x == a      = encode' (Stack (2, x)) xs
        encode' t@(Stack (n, a)) (x:xs)
          | x == a      = encode' (Stack (n + 1, x)) xs
        encode' t (x:xs)= t : encode' (Item x) xs