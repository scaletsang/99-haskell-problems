module List.Combination where

sdrop' :: Eq a => a -> [a] -> [a]
sdrop' _ [] = []
sdrop' x (y:ys) =
  if x == y then ys
  else y : sdrop' x ys

p :: Eq a => [a] -> [[a]]
p [] = []
p [x] = [[x]]
p xs = concatMap (\x -> map (x:) (p $ sdrop' x xs)) xs

--P26
c :: Int -> [a] -> [[a]]
c 0 xs = [[]]
c n xs = concatMap (\x -> map (x:) (c (n-1) xs)) xs

--P27
p4 :: Eq a => [a] -> [[a]]
p4 [] = []
p4 (x:xs) = map (\y ->
  if x == head y then []
  else x : y) (p3 xs) ++ p4 xs

p3 :: Eq a => [a] -> [[a]]
p3 [] = []
p3 (x:xs) = map (\y ->
  if x == head y then []
  else x : y) (p2 xs) ++ p3 xs

p2 :: Eq a => [a] -> [[a]]
p2 [] = []
p2 (x:xs) = map (\y ->
  if x == head y then []
  else x : y) (p1 xs) ++ p2 xs

p1 :: [a] -> [[a]]
p1 = map (: [])

-- Returns all subsets of a list of size n
pn :: Eq a => Int -> [a] -> [[a]]
pn 0 _      = []
pn _ []     = []
pn 1 xs     = map (: []) xs
pn n (x:xs) = map (x:) (pn (n-1) xs) ++ pn n xs

-- Takes 2 lists of items and returns the 2nd argument without the first occurances of the 1st argument
lsdrop'' :: Eq a => [a] -> [a] -> [a]
lsdrop'' = flip $ foldl (flip sdrop')

lsdrop :: Eq a => [a] -> [a] -> [a]
lsdrop y xs = lsdrop' y y xs
  where lsdrop' _ _ [] = []
        lsdrop' [] _ xs = xs
        lsdrop' zs [] (x:xs) = x : lsdrop' zs zs xs
        lsdrop' (z:zs) (y:ys) (x:xs) =
          if x == y then lsdrop' (sdrop' y (z:zs)) (sdrop' y (z:zs)) xs
          else lsdrop' (z:zs) ys (x:xs)
--
-- --P27a
-- g3 :: Eq a => [a] -> [[[a]]]
-- g3 xs = map (\y -> map (\z -> (y ++ z) ++ [(w y z)]) (pn 3 (lsdrop y xs))) (pn 2 xs)
--   where w y' z' = lsdrop (y' ++ z') xs

g :: Eq a => [a] -> [Int] -> [[[a]]]
g _ [] = [[]]
g xs (n:ns) = map (\(ss, z) -> concat $ map (\w -> ss:w) z) $ zip subsetsN zs
  where subsetsN = pn n xs
        remaining = map (\ss -> lsdrop'' ss xs) subsetsN
        zs = map (\x -> g x ns) remaining
--
-- nsort :: [Int] -> [Int]
-- nsort xs = foldl f [] xs
--   where f [] x = (x:)
--         f s x = map

maximum' :: Ord a => [a] -> a
maximum' [] = error "No list is taken in"
maximum' [x] = x
maximum' (x:xs) = m x (maximum' xs)
  where m x y =
          if x > y then x
          else y
