module List.Tree where

-- P07
-- Flatten a nested list structure.
-- An arbitary nested list can be described as a directed graph in Haskell, namely a tree.
data Tree a = Leaf a | Branch [Tree a] | Empty

instance Show a => Show (Tree a) where
  show Empty       = "Empty"
  show (Leaf a)    = "Leaf " ++ show a
  show (Branch xs) = "Branch [" ++ foldr f "" xs ++ "]"
    where f x [] = show x
          f x a  = show x ++ ", " ++ a

instance Semigroup (Tree a) where
  (<>) Empty       x           = x
  (<>) x           Empty       = x
  (<>) x@(Leaf _)  y@(Leaf _)  = Branch [x, y]
  (<>) x@(Leaf _)  (Branch ys) = convert . Branch $ (x:ys)
  (<>) (Branch xs) y@(Leaf _)  = convert . Branch $ xs ++ [y]
  (<>) (Branch xs) (Branch ys) = convert . Branch $ xs ++ ys

convert :: Tree a -> Tree a
convert (Branch [])  = Empty
convert (Branch [x]) = convert x
convert x = x

instance Monoid (Tree a) where
  mempty = Empty

-- Interface
branch :: [a] -> Tree a
branch []  = Empty
branch [x] = Leaf x
branch xs  = Branch . map f $ xs
  where f a = convert $ branch [a]


-- Target function
flatten :: Tree a -> Tree a
flatten (Branch xs) = foldl (<>) mempty $ map flatten xs
flatten x           = x