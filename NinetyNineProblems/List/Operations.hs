module List.Operations where
import Control.Monad.Writer

-- Tasks from https://www.ic.unicamp.br/~meidanis/courses/mc336/2006s2/funcional/L-99_Ninety-Nine_Lisp_Problems.html

-- P01
-- Find the last box of a list.
last' :: [a] -> a
last' []     = undefined
last' [x]    = x
last' (x:xs) = last' xs

-- P02
-- Find the last but one box of a list.
last2 :: [a] -> [a]
last2 []       = undefined
last2 xs@[_,_] = xs
last2 (x:xs)   = last2 xs

-- P03
-- Find the K'th element of a list.
findElem :: Int -> [a] -> a
findElem n []     = undefined
findElem 0 (x:_)  = x
findElem n (_:xs) = findElem (n-1) xs

-- P04
-- Find the number of elements of a list.
len :: [a] -> Int
len = foldl (\x _ -> x + 1) 0

-- Less efficient
len2 :: [a] -> Int
len2 = foldl (\a f -> f a) 0 . map (const (+ 1))

-- Since it folds from the right, this solution will not take advantage of lazy evaluation when the list is composed from the left, so it is not only very inefficient but also easier to have stack overflow
len3 :: [a] -> Int
len3 = foldr (const (+ 1)) 0

-- P05
-- Reverse a list.
reverse' :: [a] -> [a]
reverse' = foldl (flip (:)) []

reverse2 :: [a] -> [a]
reverse2 []     = []
reverse2 (x:xs) = reverse' xs ++ [x]

--P05 (tail recursive)
reverseR :: [a] -> [a]
reverseR = reverseR' []
  where reverseR' accu []     = accu
        reverseR' accu (x:xs) = reverseR' (x:accu) xs

-- P06
-- Find out whether a list is a palindrome.
isPalindrone :: Eq a => [a] -> Bool
isPalindrone xs = reverse' xs == xs

-- P08
-- Eliminate consecutive duplicates of list elements.
compress :: Eq a => [a] -> [a]
compress []     = []
compress (x:xs) = x : compress' x xs
  where compress' _ [] = []
        compress' accu (x:xs)
          | x == accu  = compress' accu xs
          | otherwise  = x : compress' x xs

-- P09
-- Pack consecutive duplicates of list elements into sublists.
pack :: Eq a => [a] -> [[a]]
pack = pack' []
  where pack' a  []     = [a]
        pack' [] (x:xs) = pack' [x] xs
        pack' a@(y:_) (x:xs)
          | x == y      = pack' (x:a) xs
          | otherwise   = a : pack' [x] xs

pack2 :: Eq a => [a] -> [[a]]
pack2 xs = let (a, b) = foldl f ([],[]) xs in a ++ [b]
  where f (xs, []) z  = (xs, [z])
        f (xs, a@(y:_)) z
          | y == z    = (xs, z : a)
          | otherwise = (xs ++ [a], [z])

-- P14
-- Duplicate the elements of a list.
dupli :: [a] -> [a]
dupli = foldr (\x -> (++) [x, x]) []

-- P15
-- Replicate the elements of a list a given number of times.
repli :: Int -> [a] -> [a]
repli n = concatMap (repli' n)
  where repli' 0 x = []
        repli' n x = x : repli' (n - 1) x

repli2 :: Int -> [a] -> [a]
repli2 n = foldl g []
  where g a x = a ++ replicate n x

-- P16
-- Drop every N'th element from a list.
drop' :: Int -> [a] -> [a]
drop' n = drop'' n n
    where drop'' _ _ []      = []
          drop'' 1 n (x:xs)  = drop'' n n xs
          drop'' nc n (x:xs) = x : drop'' (nc-1) n xs

-- Solution using the Writer monad, tail-head recursive (I think)
dropW :: Int -> [a] -> [a]
dropW n = reverse . execWriter . foldl f (writer (n, []))
  where f :: Writer [a] Int -> a -> Writer [a] Int
        f m x = let (n', xs) = runWriter m in
          if n' == 1 then writer (n, xs) else writer (0, [x]) >> writer (n' - 1, xs)

-- P17
--  Split a list into two parts; the length of the first part is given.
--  Do not use any predefined predicates.
splitAt' :: Int -> [a] -> [[a]]
splitAt' n xs = split' n [] xs
  where split' _ accu []     = [[]]
        split' 0 accu xs     = accu : [xs]
        split' n accu (x:xs) = split' (n-1) (accu ++ [x]) xs

-- P18
-- Extract a slice from a list.
-- Given two indices, I and K, the slice is the list containing the elements between the I'th and K'th element of the original list (both limits included). Start counting the elements with 1.

slice' :: Int -> Int -> [a] -> [a]
slice' lb ub xs
  | lb < 1    = undefined
  | ub > lb   = take (ub - lb + 1) (ndrop (lb - 1) xs)
  | ub == lb  = [findElem (lb - 1) xs]
  | otherwise = slice' ub lb xs

-- P19
-- Rotate a list N places to the left.
rotate :: Int -> [a] -> [a]
rotate n xs
  | n < 0     = rotate (length xs + n) xs
  | otherwise = drop n xs ++ take n xs

rotate2 :: Int -> [a] -> [a]
rotate2 n xs
  | n < 0     = rotate2 (length xs + n) xs
  | otherwise = let [x, y] = splitAt' n xs in y ++ x

-- P20
-- Remove the K'th element from a list.
ndrop :: Int -> [a] -> [a]
ndrop _ []     = []
ndrop 1 (_:xs) = xs
ndrop n (x:xs) = x : ndrop (n - 1) xs

-- P21
-- Insert an element at a given position into a list.
insert :: Int -> a -> [a] -> [a]
insert 1 y xs     = y : xs
insert _ _ []     = []
insert n y (x:xs) = x : insert (n - 1) y xs

-- P22
-- Create a list containing all integers within a given range.
-- If first argument is smaller than second, produce a list in decreasing order.
range :: Int -> Int -> [Int]
range n m
  | n == m = [n]
  | n < m = n : range (n+1) m
  | otherwise = n : range (n-1) m

range2 :: Int -> Int -> [Int]
range2 n1 n2
  | n1 <= n2  = [n1..n2]
  | otherwise = reverse [n2..n1]

