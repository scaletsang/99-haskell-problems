module Testing.LogicCodes where

import Testing.Testsuit
import LogicCodes.Logic
    ( showTable, 
      table, 
      table2, 
      Expr(Var, And, Impl, Or, Equ) )
import LogicCodes.Codes 
     ( gray, 
       huffsman, 
       HuffsmanList )

expr1 = And (Var 'A') (Var 'B')
expr2 = Impl (Or (Var 3) (Equ (Var 5) (Var 9))) (Var 9)

p46 :: IO Int 
p46 = subject "P46: TruthTable of 2 variables" >>=
  testLongStr (showTable $ table2 'A' 'B' expr1)

p47 :: IO Int 
p47 = p46

p48 :: IO Int 
p48 = subject "P48: TruthTable of multiple variables" >>=
  testLongStr (showTable $ table [3,5,9] expr2)

p49 :: IO Int 
p49 = subject "P49: Gray Code" >>=
  testMultiple 1 5 (+1) gray

huffsmanLs1 = [("a", 45), ("b", 13), ("c", 12), ("d", 16), ("e", 9), ("f", 5)]
huffsmanLs2 = [("a", 10), ("b", 15), ("c", 30), ("d", 16), ("e", 29)]

p50 :: IO Int
p50 = subject "P50: Huffsman Code" >>=
  test (huffsman huffsmanLs1) >>=
  test (huffsman huffsmanLs2) >>=
  test (huffsman [('a', 6)]) >>=
  test (huffsman [] :: Maybe (HuffsmanList Int))
