module Testing.Testsuit where

type Test = Int -> IO Int

red = "\ESC[1;31m"
blue = "\ESC[1;34m"
green = "\ESC[1;32m"
white = "\ESC[0m"
yellow = "\ESC[1;33m"
underline = "\ESC[0;4;1m"

subject :: String -> IO Int
subject str = do
  putStrLn $ underline ++ str ++ white
  return 1

test :: Show a => a -> Test
test = _test putStr show

testStr :: String -> Test
testStr = _test putStr id

testLong :: Show a => a -> Test
testLong = _test putStrLn show

testLongStr :: String -> Test
testLongStr = _test putStrLn id

_test :: (String -> IO ()) -> (a -> String) -> a -> Test
_test f g a n = do
  f $ "  " ++ blue ++ "test " ++ show n ++ ": " ++ green
  putStr $ g a
  putStrLn white
  return (n + 1)

testMultiple :: (Eq a, Show b) => a -> a -> (a -> a) -> (a -> b) -> Test
testMultiple lb ub g f n
  | ub == lb  = test (f lb) n
  | otherwise = test (f lb) n >>= testMultiple (g lb) ub g f

testRange :: Show a => Int -> Int -> (Int -> a) -> Test
testRange lb ub = testMultiple lb ub (+1)