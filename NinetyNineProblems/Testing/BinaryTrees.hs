module Testing.BinaryTrees where

import Testing.Testsuit
import BinaryTrees.BalancedTrees
    ( Tree(Empty, Branch), 
      branchL, 
      branchR, 
      cbalTree, 
      isSymmetric )

main = print $ isSymmetric $ head $ cbalTree 15

tree1 = Branch 'x' (Branch 'x' Empty bR) bL
  where bL = branchL 'x'
        bR = branchR 'x'