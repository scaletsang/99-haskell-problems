module Testing.List where
import Testing.Testsuit
import List.Operations
    ( last',
      last2,
      findElem,
      len,
      len2,
      len3,
      reverse',
      reverse2,
      reverseR,
      isPalindrone,
      compress,
      pack,
      pack2,
      dupli,
      repli,
      repli2,
      drop',
      dropW,
      slice',
      rotate,
      rotate2,
      ndrop,
      insert,
      range,
      range2,
      splitAt' )
import List.Tree
    ( Tree(Branch, Leaf),
      branch,
      flatten )
import List.Encode
    ( encode,
      encodeD,
      encodeModified,
      decode,
      decodeList,
      encodeList,
      encodeDirect )

p01 :: IO Int
p01 = subject "P01: Last Item of a list" >>=
  test (last' ['a'..'z']) >>=
  test (last' [(-100)..99999])

p02 :: IO Int
p02 = subject "P02: Last 2 Items of a list" >>=
  test (last2 [1..99999]) >>=
  test (last2 [1,2]) >>=
  test (last2 ["22342", "sdfsdf", "2f2"])

p03 :: IO Int
p03 = subject "P03: Find n'th element of a list" >>=
  test (findElem 5 [1..10]) >>=
  test (findElem 5 ['a'..'k']) >>=
  test (findElem 0 ['a'..'k'])

p04 :: IO Int
p04 = subject "P04: Length of a list" >>=
  test (len  [-100..1000000000]) >>=
  test (len2 [-100..1000000000]) >>=
  test (len3 [-100..100000000]) >>=
  test (len $ ['a'..'z'] ++ ['a' .. 'z'])

p05 :: IO Int
p05 = subject "P05: Reverse a list" >>=
  test (reverse' ['a'..'z']) >>=
  test (reverseR ['a'..'z']) >>=
  test (reverse2 ['a'..'z']) >>=
  test (reverse' [0..100]) >>=
  test (reverseR [0..100]) >>=
  test (reverse2 [0..100])

p06 :: IO Int
p06 = subject "P06: Check whether a list is palindrone" >>=
  test (isPalindrone $ xs ++ reverse' xs) >>=
  test (isPalindrone $ ys ++ reverse' ys) >>=
  test (isPalindrone $ xs ++ f xs) >>=
  test (isPalindrone $ ys ++ f ys)
    where f = tail . reverse'
          xs = ['a'..'k']
          ys =  [(-70)..9999]

p07 :: IO Int
p07 = subject "P07: Flatten a nested list structure" >>=
  test (flatten ws) >>=
  test (flatten xs) >>=
  test (flatten ys) >>=
  test (flatten zs)
    where ws = Branch [Leaf 1, Leaf 2, branch [3, 4]]
          xs = Branch [Leaf (Leaf 'a'),
                       Leaf (Branch [Leaf 'b']),
                       Branch [Leaf (Leaf 'c'),
                       Branch [Branch [Leaf (Leaf 'd')]]]]
          ys = branch [24376]
          zs = branch [] :: Tree Int

p08 :: IO Int
p08 = subject "P08: Eliminate consecutive duplicate items from a list" >>=
  test (compress $ replicate 10000 'a' <> replicate 9999999 'b') >>=
  test (compress $ replicate 234976 "sadsf" <> ["a", "b", "c", "c", "d", "c", "c", "e"]) >>=
  test (compress $ replicate 10000 [] <> replicate 9999999 [3])

list1 :: String
list1 = ['a', 'b'] <> replicate 10 'c' <> ['d'] <> replicate 5 'e'

list2 :: [Int]
list2 = [1, 2] <> replicate 10 3 <> [4] <> replicate 5 5

p09 :: IO Int
p09 = subject "P09: Pack consecutive duplicates into sublist" >>=
  test (pack list1) >>=
  test (pack2 list2)

p10 :: IO Int
p10 = subject "P10: Encode packing" >>=
  test (encode list1) >>=
  test (encode list2) >>=
  test (encodeD list1) >>=
  test (encodeD list2)

p11 :: IO Int
p11 = subject "P11: Encode packing (modified)" >>=
  test (encodeModified list1) >>=
  test (encodeModified list2)

p12 :: IO Int
p12 = subject "P12: Decode packing (modified)" >>=
  test (decode $ encodeModified list1) >>=
  test (decode $ encodeModified list2)

p13 :: IO Int
p13 = subject "P13: Encode packing (Direct)" >>=
  test (encodeList list1) >>=
  test (decodeList $ encodeList list1) >>=
  test (encodeList list2) >>=
  test (decodeList $ encodeList list2) >>=
  test (encodeDirect list1) >>=
  test (decode $ encodeDirect list1) >>=
  test (encodeDirect list2) >>=
  test (decode $ encodeDirect list2)

p14 :: IO Int
p14 = subject "P14: Duplicate List element" >>=
  test (dupli [1,2,3,4,45,5,6]) >>=
  test (dupli [[3],[99]])

p15 :: IO Int
p15 = subject "P15: Replicate List elements by n times" >>=
  testRange 1 10 (\x -> repli x [1,2,3,4,45,5,6]) >>=
  testRange 1 10 (\x -> repli2 x [1,2,3,4,45,5,6])

p16 :: IO Int
p16 = subject "P16: Drop every n terms from a list" >>=
  testRange 1 10 (\x -> drop' x [1..50]) >>=
  testRange 1 10 (\x -> dropW x [1..50])

p17 :: IO Int
p17 = subject "P17: Split a list into two at a given index" >>=
  testRange 0 9 (\x -> splitAt' x [1..10])

p18 :: IO Int
p18 = subject "P18: Slice a list with two given indces" >>=
  testRange 1 9 (\x -> slice' 1 x [1..10]) >>=
  testRange 1 9 (\x -> slice' 5 x [1..10]) >>=
  test (slice' 1 1 [1..10]) >>=
  test (slice' 1 2 [1..10])

p19 :: IO Int
p19 = subject "P19: Rotate a list" >>=
  testRange 0 9 (\x -> rotate x [1..10]) >>=
  testRange 0 9 (\x -> rotate2 x [1..10])

p20 :: IO Int
p20 = subject "P20: Drop n items from a list" >>=
  testRange 0 9 (\x -> ndrop x ['a'..'z'])

p21 :: IO Int
p21 = subject "P21: Insert an item to a list at a given position" >>=
  testRange 1 21 (\x -> insert x 'Z' (replicate 20 '_'))

p22 :: IO Int
p22 = subject "P22: Generate a range of Integers, represented in list" >>=
  testRange 1 10 (`range` 5) >>=
  testRange 1 10 (`range2` 5)