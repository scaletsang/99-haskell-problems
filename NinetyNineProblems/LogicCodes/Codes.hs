module LogicCodes.Codes where

-- P49
-- An n-bit Gray code is a sequence of n-bit strings constructed according to certain rules.

gray :: Int -> [String]
gray 1 = ["0", "1"]
gray n = foldl f [] $ gray (n - 1)
  where f a  xs = a ++ map (g xs) (gray 1)
        g xs ys = xs ++ ys

-- P50
-- We suppose a set of symbols with their frequencies, given as a list of fr(S,F) terms. Example: [fr(a,45),fr(b,13),fr(c,12),fr(d,16),fr(e,9),fr(f,5)]. Our objective is to construct a list hc(S,C) terms, where C is the Huffman code word for the symbol S. In our example, the result could be Hs = [hc(a,'0'), hc(b,'101'), hc(c,'100'), hc(d,'111'), hc(e,'1101'), hc(f,'1100')] [hc(a,'01'),...etc.]. The task shall be performed by the predicate huffman/2.

data TreeH a = Char Int a | Node Int (TreeH a) (TreeH a)
type FreqList a = [(a, Int)]
type HuffsmanList a = [(a, String)]

instance Show a => Show (TreeH a) where
  show tree = showT 0 tree
    where showT n (Char f x)   = indent n ++ unwords ["(Char", show f, show x] ++ ")"
          showT n (Node f l r) = indent n ++ unwords ["(Node", show f, showT (n + 4) l, showT (n + 4) r] ++ ")"
          indent n = '\n' : replicate n ' '

--------------------------
-- TreeH type interface --
--------------------------

getFreq :: TreeH a -> Int
getFreq (Char n _)   = n
getFreq (Node n _ _) = n

addNode :: TreeH a -> TreeH a -> TreeH a
addNode node tree
  | nn > nt   = Node (nn + nt) tree node
  | otherwise = Node (nn + nt) node tree
    where nn = getFreq node
          nt = getFreq tree

----------------------
-- Target functions --
----------------------

huffsman :: FreqList a -> Maybe (HuffsmanList a)
huffsman fs = fromFreqList fs >>= f
  where f (Char n x) = Just [(x, "0")]
        f tree       = Just $ traverseH tree ""

fromFreqList :: FreqList a -> Maybe (TreeH a)
fromFreqList = makeTreeH . sortCharList . map f
  where f (x, n) = Char n x

--------------------
-- Implementation --
--------------------

traverseH :: TreeH a -> String -> HuffsmanList a
traverseH (Char n x)   str = [(x, str)]
traverseH (Node n l r) str = traverseH l (str ++ "0") ++ traverseH r (str ++ "1")

makeTreeH :: [TreeH a] -> Maybe (TreeH a)
makeTreeH []     = Nothing
makeTreeH [x]    = Just x
makeTreeH (x:y:xs) = Just (sortTreeHList (addNode x y) xs) >>= makeTreeH
  where sortTreeHList node []     = [node]
        sortTreeHList node ls@(x:xs)
          | getFreq node <= getFreq x = node : ls
          | otherwise = x : sortTreeHList node xs

sortCharList :: [TreeH a] -> [TreeH a]
sortCharList [] = []
sortCharList (x:xs) = f (<=) ++ [x] ++ f (>)
  where f g = sortCharList [y | y <- xs, g (getFreq y) (getFreq x)]