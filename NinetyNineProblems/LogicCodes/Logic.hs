module LogicCodes.Logic where

-- P46 & P47
-- Define predicates and/2, or/2, nand/2, nor/2, xor/2, impl/2 and equ/2 (for logical equivalence) which succeed or fail according to the result of their respective operations; e.g. and(A,B) will succeed, if and only if both A and B succeed.
-- A logical expression in two variables can then be written as in the following example: and(or(A,B),nand(A,B)).
-- Now, write a predicate table/3 which prints the truth table of a given logical expression in two variables.

-- P48
-- Generalize problem P47 in such a way that the logical expression may contain any number of logical variables. Define table/2 in a way that table(List,Expr) prints the truth table for the expression Expr, which contains the logical variables enumerated in List.

data Expr a = And  (Expr a) (Expr a) |
              Or   (Expr a) (Expr a) |
              Nand (Expr a) (Expr a) |
              Nor  (Expr a) (Expr a) |
              Xor  (Expr a) (Expr a) |
              Impl (Expr a) (Expr a) |
              Equ  (Expr a) (Expr a) |
              Var  a
                deriving (Show, Eq)

type Bindings a = [(a, Bool)]
type Table a = [(Bindings a, Maybe Bool)]

------------------------
--   Target functions
------------------------

table :: Eq a => [a] -> Expr a -> Table a
table xs expr = map f $ permute (length xs)
  where f ys = (makeBindings ys, calc (makeBindings ys) expr)
        makeBindings = zip xs

table2 :: Eq a => a -> a -> Expr a -> Table a
table2 a b = table [a, b]

-------------------
--   Printing
-------------------

showTable :: Table a -> String
showTable = foldl f ""
  where f _ (_, Nothing) = "Error: invalid variables found in expression"
        f a (bs, Just x) = a ++ showBindings bs ++ " = " ++ show x ++ "\n"

showBindings :: Bindings a -> String
showBindings = unwords . map f
  where f (_, True) = "True "
        f (_, x)    = show x

permute :: Int -> [[Bool]]
permute 1 = [[True], [False]]
permute n = foldl f [] $ permute (n - 1)
  where f a  xs  = a ++ map (g xs) (permute 1)
        g xs x = head x : xs

calc :: Eq a => Bindings a -> Expr a -> Maybe Bool
calc bs expr = substitute bs expr >>= Just . eval

substitute :: Eq a => Bindings a -> Expr a -> Maybe (Expr Bool)
substitute bs expr = f bs expr
  where f bs expr    = case expr of
          (Var  x)   -> getBinding bs x >>= (Just . Var)
          (And  x y) -> g bs And x y
          (Or   x y) -> g bs Or x y
          (Nand x y) -> g bs Nand x y
          (Nor  x y) -> g bs Nor x y
          (Xor  x y) -> g bs Xor x y
          (Impl x y) -> g bs Impl x y
          (Equ  x y) -> g bs Equ x y
        g bs expr x y = do
          a <- f bs x
          b <- f bs y
          Just (expr a b)

getBinding :: Eq a => Bindings a -> a -> Maybe Bool
getBinding [] x = Nothing
getBinding ((x,b):xs) y
  | x == y      = Just b
  | otherwise   = getBinding xs y

eval :: Expr Bool -> Bool
eval (Var  x)   = x
eval (Equ  x y) = eval x == eval y
eval (And  x y) = eval x && eval y
eval (Or   x y) = eval x || eval y
eval (Nand x y) = not $ eval (And x y)
eval (Nor  x y) = not $ eval (Or x y)
eval (Xor  x y) = eval (Or x y) && not (eval (Equ x y))
eval (Impl x y) = not (eval x && not (eval y))
