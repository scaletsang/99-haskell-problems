# 99 Haskell Problems
This is my attempt to implement all if not most of the questions from [this page](https://www.ic.unicamp.br/~meidanis/courses/mc336/2006s2/funcional/L-99_Ninety-Nine_Lisp_Problems.html).

Originally I started learning programming with Lisp, and I was following along the same problem set. Later I fall in love with Haskell so I decided to practice my beloved with this problem set.

## How to run the program?
I have implemented test cases for each solution. A set of test cases for one solution is named under its question number, e.g. P01 incldues all the testcases for question 1.

Uncomment the testcase set of the question you want to run in [app/Main.hs](/app/Main.hs).

Then run:  
```bash
cabal run
```

See all the testcases under the Testing folder: [NinetyNineProblems/Testing](/NinetyNineProblems/Testing)